#ifndef MyEventAction_h
#define MyEventAction_h

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4Timer.hh"
#include "MyPseudoHit.hh"

class MyRunAction;
class MySteppingAction;
struct MyPseudoHitContainer;


class MyEventAction: public G4UserEventAction {

public:

  MyEventAction();
  ~MyEventAction();

  virtual void BeginOfEventAction( const G4Event* evt );
  virtual void EndOfEventAction( const G4Event* evt );

  void SetSteppingAction(MySteppingAction* sa) { theSteppingAction = sa; }
  void SetRunAction( MyRunAction* ra) { theRunAction = ra; }
  void MemoryProfile( G4bool flag ) { memoryProfile = flag; }

private:

  MyRunAction* theRunAction;
  MySteppingAction* theSteppingAction;
  G4Timer timing;
  MyPseudoHitContainer* phc;
  G4bool memoryProfile;
};

#endif
