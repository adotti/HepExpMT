#ifndef MYUSERACTIONINIT_HH
#define MYUSERACTIONINIT_HH

#include "G4VUserActionInitialization.hh"
#include "G4Types.hh"

/// The class which initializes the actions for master and worker threads.
class MyUserActionInitialization : public G4VUserActionInitialization {
public:
  MyUserActionInitialization(const G4bool memprof,
			     const G4bool randomgun,
			     const unsigned long maxnumhits);
  void Build() const;
  void BuildForMaster() const;
private:
  const G4bool memprof;
  const G4bool randomgun;
  const unsigned int maxnumhits;
};

#endif
