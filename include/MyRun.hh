#ifndef MYRUN_HH
#define MYRUN_HH

#include "G4Run.hh"
#include "MyPseudoHit.hh"

class MyRun : public G4Run
{
public:
  MyRun();
  virtual void Merge(const G4Run*);
  void UpadteForEvent( const G4double edep ) {
    theSumOfTotalDepositedEnergy += edep;
  }
  double GetTotEdep() const { return theSumOfTotalDepositedEnergy; }
  MyPseudoHitContainer* GetPseudoHitContainer() { return &pseudohit; }
private:
  double theSumOfTotalDepositedEnergy;
  MyPseudoHitContainer pseudohit;
};

#endif
