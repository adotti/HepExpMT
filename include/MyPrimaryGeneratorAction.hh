#ifndef MyPrimaryGeneratorAction_h
#define MyPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include <string>

class G4ParticleTable;
class G4Event;
class G4GeneralParticleSource;


class MyPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {

public:

  MyPrimaryGeneratorAction(const bool randomize = true);
  ~MyPrimaryGeneratorAction();

public:

  void GeneratePrimaries( G4Event* anEvent );

private:

  G4GeneralParticleSource* particleGun;
  G4ParticleTable* particleTable;

  const bool randomize;

};

#endif


