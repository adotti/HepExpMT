/*
 * MyPseudoHit.hh
 *
 *  Created on: Aug 12, 2015
 *      Author: adotti
 */

#ifndef INCLUDE_MYPSEUDOHIT_HH_
#define INCLUDE_MYPSEUDOHIT_HH_
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4UnitsTable.hh"
#include <vector>
#include <iostream>
#include <string>
#include <utility>

struct MyPseudoHitContainer {
  typedef std::vector<G4double> Vector;
  typedef Vector::size_type size_type;
  static unsigned long maximumsize;
  Vector x;
  Vector y;
  Vector z;
  Vector t;
  Vector edep;

  void clear() {
    x.clear();
    y.clear();
    z.clear();
    t.clear();
    edep.clear();
  }

  void push_back( const G4ThreeVector& pos , G4double time, G4double ene) {
    x.push_back(pos.x());
    y.push_back(pos.y());
    z.push_back(pos.z());
    t.push_back(time);
    edep.push_back(ene);
  }

  size_type size() const { return edep.size(); }

  size_type buffer_size() const { return size()*5; }

  void pack(G4double*);

  void unpack(G4double*,size_type size);

  unsigned int bytes() const { return buffer_size()*sizeof(G4double); }

};

inline std::ostream& operator<<(std::ostream& os , const MyPseudoHitContainer& hc) {
  os << "[ ";
  for ( MyPseudoHitContainer::size_type idx = 0 ; idx<hc.size() ; ++idx) {
    os << "{x=" << G4BestUnit(hc.x[idx],"Length")
       << "," << G4BestUnit(hc.y[idx],"Length")
       << "," << G4BestUnit(hc.z[idx],"Length")
       << "; t=" << G4BestUnit(hc.t[idx],"Time")
       << "; Edep=" << G4BestUnit(hc.edep[idx],"Energy") << "}, ";
  }
  os << " ]";
  return os;
}

#endif /* INCLUDE_MYPSEUDOHIT_HH_ */
