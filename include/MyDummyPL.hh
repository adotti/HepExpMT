//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: MyDummyPL.hh 66892 2015-09-21 13:50:00Z adotti $
//
//---------------------------------------------------------------------------
//
// ClassName: MyDummyPL
//
// Author: 2015  Andrea Dotti
//   created from examples/extended/eventgenerator/exgps
//
// Modified:
//
//----------------------------------------------------------------------------
//
#ifndef DUMMYPL_HH
#define DUMMYPL_HH
#include "G4VUserPhysicsList.hh"

//Dummy physics list with the minimal possible content
//Used to test w/o physics the application
class MyDummyPL: public G4VUserPhysicsList
{
  public:
  MyDummyPL();
   ~MyDummyPL();

  protected:
    // Construct particle and physics
    virtual void ConstructParticle();
    virtual void ConstructProcess();
    virtual void SetCuts();
};
#endif



