#ifndef MyRunAction_h
#define MyRunAction_h

#include "G4UserRunAction.hh"
#include "MyRun.hh"
#include "MyNtuple.hh"

class G4Run;
class G4Event;


class MyRunAction: public G4UserRunAction {

public:

  MyRunAction();
  virtual ~MyRunAction();

  virtual void BeginOfRunAction( const G4Run* aRun );
  virtual void EndOfRunAction( const G4Run* aRun );

  void UpdateEndOfEvent( const double eventTotalDepositedEnergy , const G4Event* );

  virtual G4Run* GenerateRun();

private:
  MyRun* currentRun;
  MyNtuple iont;
};

#endif
