#ifndef MYNTUPLE_HH
#define MYNTUPLE_HH

#include "MyPseudoHit.hh"
#include "G4String.hh"
class MyRun;
class G4Event;

#ifdef HEPEXPMT_WITH_ROOT
class TFile;
class TTree;
#endif

class MyNtuple {
public:
  MyNtuple() : hits(0) ,filename_base("HepExpMT"),ntuplename("PseudoHits") {
#ifdef HEPEXPMT_WITH_ROOT
    root_file = 0;
    root_tree = 0;
    sizewritten = 0L;
#   ifdef HEPEXPMT_WITH_TSCIF
    sizetosend = 0L;
#   endif //TSCIF
#endif //ROOT
  }
  void SetPseudoHits( MyPseudoHitContainer* phc ) { hits = phc; }
  void EndOfEvent( const G4Event* );
  void BeginOfRun( const MyRun* );
  void EndOfRun( const MyRun* );
  void SetFileName( const G4String& fn ) { filename_base = fn; }
  void SetNtName( const G4String& nn ) { ntuplename = nn; }
#ifdef HEPEXPMT_WITH_TSCIF
  static void SetChunkSize( const long value ) { chunkSize = value; }
  static long GetChunkSize() { return chunkSize; }
#endif
private:
  MyPseudoHitContainer* hits;
  G4String filename_base;
  G4String ntuplename;
#ifdef HEPEXPMT_WITH_ROOT
  TFile* root_file;
  TTree* root_tree;
  long sizewritten;
# ifdef HEPEXPMT_WITH_TSCIF
  long sizetosend;
  static long chunkSize;
# endif //TSCIF
#endif //ROOT
};

#endif
