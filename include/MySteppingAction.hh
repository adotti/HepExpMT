#ifndef MySteppingAction_H
#define MySteppingAction_H 1

#include "globals.hh"
#include "G4UserSteppingAction.hh"
#include "G4SystemOfUnits.hh"
struct MyPseudoHitContainer;
class G4Track;

//Implement logic of track killing if number of steps is too large
//(looping particles)
struct TrackKiller {
  TrackKiller() : nstepsLim(1000000),
      stepLengthLim(1*mm),
      currentTrackId(0),
      lastStepL{0.,0.,0.,0.,0.,0.,0.,0.,0.,0.},
      avgStepL(0.) {}
  G4bool kill(const G4Track*);
  const G4int nstepsLim;
  const G4double stepLengthLim;
  G4int currentTrackId;
  G4double lastStepL[10];
  G4double avgStepL;
};


class MySteppingAction : public G4UserSteppingAction {

public:

  MySteppingAction(G4bool hitsScoring);
  virtual ~MySteppingAction();

public:

  virtual void UserSteppingAction( const G4Step* );
  // The main method to define.

  inline G4double getTotalEdepAllParticles() const;
  // Total deposited energy, in the same event,
  // due to all particles, in the whole experimental hall.

  inline void reset();
  // This method should be invoked at the end of the event,
  // to reset to zero the total deposit energy variables.

private:

  G4double totalEdepAllParticles;
  long long stepNumber;
  const G4bool hitsScoring;
  TrackKiller killer;
};


inline G4double MySteppingAction::getTotalEdepAllParticles() const {
  return totalEdepAllParticles;
}


inline void MySteppingAction::reset() {
  totalEdepAllParticles = 0.0;
}


#endif


