#ifndef MyEventActionSimple_h
#define MyEventActionSimple_h

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4Timer.hh"
#include "MyPseudoHit.hh"

class MyRunAction;
class MySteppingAction;


class MyEventActionSimple: public G4UserEventAction {

public:

  MyEventActionSimple() {}
  ~MyEventActionSimple() {}

  virtual void BeginOfEventAction( const G4Event* evt );
  virtual void EndOfEventAction( const G4Event* evt );
private:
  G4Timer timing;
};

#endif
