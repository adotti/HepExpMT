struct meminfo_t {
  int vsize;
  int rss;
  int size;
};


#include <string>
#include <fstream>
#include <sstream>

#include <stdio.h>

inline std::string exec(const std::string& command) {
  //char* line = NULL;
  // Following initialization is equivalent to char* result = ""; and just
  // initializes result to an empty string, only it works with
  // -Werror=write-strings and is so much less clear.
  //char* result = (char*) calloc(1, 1);
  //size_t len = 0;
  std::stringstream result;

  fflush(NULL);
  FILE* fp = popen(command.c_str(), "r");
  if (fp == NULL) {
    return result.str();
  }

  char* line = NULL;
  size_t len = 0;
  while(getline(&line, &len, fp) != -1) {
    //char* result = new char[len+1];
    // +1 below to allow room for null terminator.
    //result = (char*) realloc(result, strlen(result) + strlen(line) + 1);
    // +1 below so we copy the final null terminator.
    //strncpy(result + strlen(result), line, strlen(line) + 1);
    result<<line;
    delete line;
    line = NULL;
  }

  fflush(fp);
  if (pclose(fp) != 0) {
    //perror("Cannot close stream.\n");
  }
  return result.str();
}


#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
 
inline meminfo_t getmem() {
  pid_t pid = getpid();
  char cmd[64];
  snprintf(cmd, 64, "/bin/ps -p %d -o vsize,rss,size", pid);
  std::string result = exec(cmd);
  if ( result.size() == 0 ) {
    return meminfo_t();
  }
  std::stringstream ss;
  ss<<result;
  std::string titles[3];
  meminfo_t mem;
  ss >> titles[0] >> titles[1] >> titles[2]
     >> mem.vsize >> mem.rss >> mem.size;

  return mem;
}


