#include "MyUserActionInitialization.hh"
#include "MyPrimaryGeneratorAction.hh"
#include "MyEventAction.hh"
#include "MyEventActionSimple.hh"
#include "MyRunAction.hh"
#include "MySteppingAction.hh"
#include "MyPseudoHit.hh"

MyUserActionInitialization::MyUserActionInitialization(const G4bool flag,
                                                       const G4bool flag2,
                                                       const unsigned long flag3)
  : memprof(flag), randomgun(flag2), maxnumhits(flag3) {
  MyPseudoHitContainer::maximumsize = maxnumhits;
}

void MyUserActionInitialization::Build() const {
  SetUserAction( new MyPrimaryGeneratorAction(randomgun) );
#ifdef HEPEXPMT_MINIMAL
  SetUserAction ( new MyEventActionSimple );
#else
  //Run
  MyRunAction* ra = new MyRunAction;
  SetUserAction( ra );
  //Event
  MyEventAction* ea = new MyEventAction;
  SetUserAction( ea );
  ea->SetRunAction( ra );
  ea->MemoryProfile(memprof);
  MySteppingAction* sa = new MySteppingAction( bool(maxnumhits>0) );
  SetUserAction( sa );
  ea->SetSteppingAction(sa);
#endif
}

void MyUserActionInitialization::BuildForMaster() const {
  SetUserAction( new MyRunAction );
}
