#include "MyEventAction.hh"
#include "G4ios.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4RunManager.hh"
#include "MyRunAction.hh"
#include "MySteppingAction.hh"
#include "G4SystemOfUnits.hh"

#include "memory.hh"
#include "G4Threading.hh"

#ifdef HEPEXPMT_WITH_IGPRF
#include <dlfcn.h>
#include <ostream>
#include "G4AutoLock.hh"
void (*dump_)(const char*);
static G4Mutex init_m = G4MUTEX_INITIALIZER;
#endif


// Constructor
MyEventAction::MyEventAction() :
  theRunAction( 0 ), theSteppingAction( 0 ), phc(0) , memoryProfile(false) {

  //instanciateRunAction();
  //instanciateSteppingAction();

#ifdef HEPEXPMT_WITH_IGPRF
  G4AutoLock l(&init_m);
  if ( ! dump_ ) {
    if ( void *sym = dlsym(0, "igprof_dump_now") ) {
      dump_ = __extension__ (void(*)(const char*))sym;
    }
  }
#endif
}


// Destructor
MyEventAction::~MyEventAction() {}


// Called at the beginniing of every event
void MyEventAction::BeginOfEventAction( const G4Event* ) {
  timing.Start();
  if ( MyPseudoHitContainer::maximumsize > 0 ) {
      MyRun* run = static_cast<MyRun*>
        (G4RunManager::GetRunManager()->GetNonConstCurrentRun());
      phc = run->GetPseudoHitContainer();
      phc->clear();
  }
  //G4cout << "\n---> Begin of event: " << evt->GetEventID() << G4endl;
}


// Called at the end of every event
void MyEventAction::EndOfEventAction( const G4Event* evt ) {
  // Including IO in the event timer
  //timing.Stop();

#ifdef HEPEXPMT_WITH_IGPRF
  if ( dump_ && evt->GetEventID() % 100 == 0 ) {
    G4AutoLock l(&init_m);
    std::ostringstream fn;
    fn << "| gzip -c > igprof_evt" << evt->GetEventID() << ".mp.gz";
    dump_( fn.str().c_str() );
  }
#endif

  // First N threads will sample the process memory at every event.
  const int nProfWorkers = 10;
  if ( memoryProfile && G4Threading::G4GetThreadId() < nProfWorkers ) {
    time_t now = time(0);
    char* dt = ctime(&now);
    dt[strlen(dt)-1] = '\0';
    meminfo_t mem = getmem();
    G4cout << "xxOO0OOxxMEM " << dt << " (VSIZE,RSS,SIZE)="
           << mem.vsize << "," << mem.rss << "," << mem.size << G4endl;
  }

  if ( theSteppingAction ) {
    const G4double totalEdepAllParticles = theSteppingAction->getTotalEdepAllParticles();
    if ( evt->GetEventID() % 100 == 0 ) {
      G4cout << " ---  MyEventAction::EndOfEventAction  ---    event = "
             << evt->GetEventID() << "  Edep=" << totalEdepAllParticles/GeV
             << " GeV" << G4endl;
    }
    if (theRunAction ) theRunAction->UpdateEndOfEvent( totalEdepAllParticles , evt );
    theSteppingAction->reset();
  }

  timing.Stop();
  if ( MyPseudoHitContainer::maximumsize>0 ) {
    G4cout << "[TIME] Event " << evt->GetEventID() << " : " << timing
           << " Number of Hits: " << phc->size()
           << " (" << phc->bytes() << " Bytes)" << G4endl;
  } else {
    G4cout << "[TIME] Event " << evt->GetEventID() << " : " << timing << G4endl;
  }

}
