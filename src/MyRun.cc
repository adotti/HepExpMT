#include "MyRun.hh"

MyRun::MyRun() :  theSumOfTotalDepositedEnergy(0.) {}

void MyRun::Merge(const G4Run* other ) {
  const MyRun* worker = static_cast<const MyRun*>(other);
  theSumOfTotalDepositedEnergy += worker->theSumOfTotalDepositedEnergy;
  G4Run::Merge(other);
}
