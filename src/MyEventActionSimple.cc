#include "MyEventActionSimple.hh"
#include "G4Event.hh"


void MyEventActionSimple::BeginOfEventAction( const G4Event* ) {
  timing.Start();
}


void MyEventActionSimple::EndOfEventAction( const G4Event* evt ) {
  timing.Stop();
  G4cout << "[TIME] Event " << evt->GetEventID() << " : " << timing << G4endl;
}

