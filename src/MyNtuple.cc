#include "MyNtuple.hh"
#include "MyRun.hh"
#include "G4Event.hh"
#include "G4String.hh"
#include "G4Threading.hh"
#include <iostream>
#include <assert.h>

#ifdef HEPEXPMT_WITH_ROOT
#  ifdef HEPEXPMT_WITH_TSCIF
#    include "TSCIFFile.h"
   long MyNtuple::chunkSize = 1000000L;
#  endif
#  include "G4AutoLock.hh"
#  include "TTree.h"
#  include "TFile.h"
   namespace {
     G4Mutex m_TFileMutex = G4MUTEX_INITIALIZER;
   }
#endif //ROOT



void MyNtuple::EndOfEvent( const G4Event* ) {
#ifdef HEPEXPMT_WITH_ROOT
  const long datawr = root_tree->Fill();
  sizewritten += datawr;
# ifdef HEPEXPMT_WITH_TSCIF
  sizetosend += datawr;
  if ( sizetosend > chunkSize ) {

      int nTreeBytes = 0;
      int nFileBytes = 0;

      //Not sure why I need a lock...., Romain did not use it
      {
        G4AutoLock l(&m_TFileMutex);
        //nTreeBytes = root_tree->Write();
        nFileBytes = root_file->Write();
      }
      G4cout << "Called TSCIFFile Write, sizetosend=" << sizetosend
             << " (chunkSize=" << chunkSize << "), TTree::Write returned: "
             << nTreeBytes << ", TFile::Write returned: "
             << nFileBytes << G4endl;
      sizetosend = 0L;
  }
# endif
#endif
}

void MyNtuple::BeginOfRun( const MyRun* aRun) {
  assert( hits != 0);
  const int rid = aRun->GetRunID();
  std::ostringstream fn , nn;
  fn << filename_base << "_Run"<<rid;
#ifdef HEPEXPMT_WITH_ROOT
  sizewritten = 0L;
  const int tid = G4Threading::G4GetThreadId();
  if ( tid >=0 ) fn<<"_Tid"<<tid;//For workers add thread id
  fn<<".root"; //
  const G4String fname = fn.str();
  // We need to lock this operation (thread-safety issues in ROOT)
  {
    G4AutoLock l(&m_TFileMutex);
#   ifdef HEPEXPMT_WITH_TSCIF
    // Open the client-side TSCIF file
    // Calculate local port number using thread-id as an offset
    const int localPort = 9100 + G4Threading::G4GetThreadId();
    root_file = new TSCIFFile("test", 0, 9090, localPort);
# else //NO TSCIF
  root_file = TFile::Open( fname , "RECREATE");
# endif //TSCIF
  }
  root_tree = new TTree( ntuplename , ntuplename );
  root_tree->Branch("x",&(hits->x));
  root_tree->Branch("y",&(hits->y));
  root_tree->Branch("z",&(hits->z));
  root_tree->Branch("t",&(hits->t));
  root_tree->Branch("edep",&(hits->edep));
#else //NO ROOT
  //TODO: add here g4analysis
#endif
}

void MyNtuple::EndOfRun( const MyRun* ) {
#ifdef HEPEXPMT_WITH_ROOT
  const int nbytes = root_file->Write();
  G4cout<<"Called TFile Write, end of run, returned: "<<nbytes<<G4endl;
  // We need to lock this operation (thread-safety issues in ROOT)
  G4AutoLock l(&m_TFileMutex);
  root_file->Close();
#else //NO ROOT
  //TODO: g4analysis
#endif //ROOT
}
