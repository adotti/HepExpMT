#include "MySteppingAction.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4SystemOfUnits.hh"
#include "MyPseudoHit.hh"
#include "tls.hh"
#include "MyRun.hh"
#include "G4RunManager.hh"

G4bool TrackKiller::kill(const G4Track* trk ) {
  if ( trk->GetTrackID() != currentTrackId ) {
      currentTrackId = trk->GetTrackID();
      return false;
  }
  if ( trk->GetCurrentStepNumber() > nstepsLim ) {
      if ( trk->GetCurrentStepNumber() > 10*nstepsLim ) return true;
      const auto sn = trk->GetCurrentStepNumber();
      lastStepL[sn%10]=trk->GetStep()->GetStepLength();
      G4double avg = 0;
      for ( unsigned int i = 0 ; i < 10 ; ++i ) avg+=lastStepL[i];
      if ( avg/10 < stepLengthLim ) {
          avgStepL = avg/10;
          return true;
      }
  }
  return false;
}

// Constructor
MySteppingAction::MySteppingAction(G4bool flag) :
  totalEdepAllParticles( 0.0 ), stepNumber(0.), hitsScoring(flag),
  killer()
{}


// Destructor
MySteppingAction::~MySteppingAction() {}


// Called at every step of every particle
void MySteppingAction::UserSteppingAction( const G4Step * theStep ) {

  //Avoid looping particles
  if ( killer.kill(theStep->GetTrack()) ) {
      const auto trk= theStep->GetTrack();
      G4ExceptionDescription msg;
      msg<<"Killing track ID: "<<trk->GetTrackID()<<" ("
          <<trk->GetParticleDefinition()->GetParticleName()<<" Ekin="
          <<trk->GetKineticEnergy()/MeV<<" MeV) "
          <<"Momentum direction: "<<trk->GetMomentumDirection()
          <<" in:"<<theStep->GetPreStepPoint()->GetPhysicalVolume()->GetName()
	  <<" Material: "<<trk->GetMaterial()->GetName()
          <<" after: "<<killer.nstepsLim<<" steps, the last 10 steps have an "\
          "average length of: "<<killer.avgStepL/mm<<" mm";
      G4Exception("MySteppingAction::UserSteppingAction","step001",JustWarning,msg);
      theStep->GetTrack()->SetTrackStatus(fStopAndKill);
  }


  // Consider the total energy depositions for any
  // particle anywhere in the whole experimental hall.
  // Notice that for simplicity we are not considering
  // the energy deposit inside the sensitive parts of
  // the detector, or even inside the whole detector,
  // but overall in the experimental hall.

  const G4double edep = theStep->GetTotalEnergyDeposit();
  if ( hitsScoring && MyPseudoHitContainer::maximumsize > 0 ) {
      static G4ThreadLocal MyPseudoHitContainer* phc = 0;
      if ( phc == 0 ) {
          MyRun* run = static_cast<MyRun*>(
              G4RunManager::GetRunManager()->GetNonConstCurrentRun());
          phc = run->GetPseudoHitContainer();
      }

      if ( phc->edep.size() < MyPseudoHitContainer::maximumsize ) {
          const G4ThreeVector pos =
              .5*(theStep->GetPostStepPoint()->GetPosition() + theStep->GetPreStepPoint()->GetPosition());
          const G4double t =
              .5*(theStep->GetPostStepPoint()->GetGlobalTime() + theStep->GetPreStepPoint()->GetGlobalTime());
          //G4cout<<"TEST: Pos="<<pos/mm<<" t="<<t/s<<" E="<<edep/MeV<<G4endl;
          phc->push_back(pos,t,edep);
      }
  }
  if ( edep > 0. ) totalEdepAllParticles += edep;

}
