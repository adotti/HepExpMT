/*
 * MyPseudoHit.cc
 *
 *  Created on: Aug 12, 2015
 *      Author: adotti
 */
#include "MyPseudoHit.hh"
#include <assert.h>

unsigned long MyPseudoHitContainer::maximumsize = 100000L;



void MyPseudoHitContainer::pack(G4double* buffer) {
  for ( size_type idx = 0 ; idx<5*size() ; idx+=5){
      buffer[idx] = x[idx];
      buffer[idx+1] = y[idx];
      buffer[idx+2] = z[idx];
      buffer[idx+3] = t[idx];
      buffer[idx+4] = edep[idx];
  }
}

void MyPseudoHitContainer::unpack(G4double* buffer, MyPseudoHitContainer::size_type bufsize) {
  assert( bufsize % 5 == 0 );
  size_type numhits = bufsize/5;
  for ( size_type idx = 0 ; idx < numhits ; ++idx ) {
      x[idx] = buffer[idx];
      y[idx] = buffer[idx+1];
      z[idx] = buffer[idx+2];
      t[idx] = buffer[idx+3];
      edep[idx] = buffer[idx+4];
  }
}
