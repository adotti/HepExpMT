#include "MyRunAction.hh"
#include "globals.hh"
#include "G4Run.hh"
#include "G4SystemOfUnits.hh"
#include "G4Threading.hh"
#include <ctime>
#include "G4Timer.hh"

#include "memory.hh"

MyRunAction::MyRunAction() : currentRun(0)
{}


MyRunAction::~MyRunAction() {}

namespace {
  G4Timer m_t;
}

G4Run* MyRunAction::GenerateRun() {
  currentRun = new MyRun;
  return currentRun;
}

void MyRunAction::BeginOfRunAction( const G4Run* aRun ) {
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;
  if ( ! G4Threading::IsWorkerThread() )
  {
    time_t now = time(0);
    char* dt = ctime(&now);
    G4cout << "xxOO0OOxx " << dt;
    G4cout << "xxOO0OOxx Start of Run: " << aRun->GetRunID() << G4endl;
    meminfo_t mem = getmem();
    G4cout << "xxOO0OOxx Memory usage (VSIZE,RSS,SIZE)= "
           << mem.vsize << "," << mem.rss << "," << mem.size << G4endl;
    m_t.Start();
  }
  if ( MyPseudoHitContainer::maximumsize > 0 ) {
    iont.SetPseudoHits( currentRun->GetPseudoHitContainer() );
    iont.BeginOfRun( static_cast<const MyRun*>(aRun) );
  }
}


void MyRunAction::EndOfRunAction( const G4Run* aRun ) {
#ifndef HEPEXPMT_MINIMAL
  double average = 0.0;
  const MyRun* rr = static_cast<const MyRun*>(aRun);
  const int theNumberOfEvents = rr->GetNumberOfEvent();
  const double theSumOfTotalDepositedEnergy = rr->GetTotEdep();
  if ( theNumberOfEvents > 0 ) {
    average =  theSumOfTotalDepositedEnergy / static_cast< double >( theNumberOfEvents );
  }
  G4cout << G4endl << G4endl
         << " ###############  Final Statistics  ############### " << G4endl
         << "   Average deposited energy = " << average/GeV << " GeV" << G4endl
         << " ################################################## " << G4endl << G4endl;
#endif
  if ( ! G4Threading::IsWorkerThread() )
  {
    m_t.Stop();
    time_t now = time(0);
    char* dt = ctime(&now);
    G4cout << "xxOO0OOxx " << dt;
    G4cout << "xxOO0OOxx End of Run: " << aRun->GetRunID() << " With "
           << aRun->GetNumberOfEvent() << " events" << G4endl;
    G4cout << "xxOO0OOxx Timing info: " << m_t << G4endl;
    meminfo_t mem = getmem();
    G4cout << "xxOO0OOxx Memory usage (VSIZE,RSS,SIZE)= "
           << mem.vsize << "," << mem.rss << "," << mem.size << G4endl;
  }
  if ( MyPseudoHitContainer::maximumsize > 0 ) {
    iont.EndOfRun(static_cast<const MyRun*>(aRun));
  }

}


void MyRunAction::UpdateEndOfEvent( const double eventTotalDepositedEnergy, const G4Event* evt ) {
  currentRun->UpadteForEvent( eventTotalDepositedEnergy );
  iont.EndOfEvent(evt);
}
