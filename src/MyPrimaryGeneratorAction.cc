#include "MyPrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4GeneralParticleSource.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"
#include "Randomize.hh"
#include "G4RandomDirection.hh"
#include <vector>
#include <string>
#include "G4SystemOfUnits.hh"

namespace {
	const int numberCandidateParticles = 29;
	const std::string
		nameParticlesVector[ numberCandidateParticles ] = {
				"mu-",
				"mu+",
				"e-",
				"e+",
				"gamma",
				"pi-",
				"pi+",
				"kaon-",
				"kaon+",
				"kaon0L",
				"neutron",
				"proton",
				"anti_neutron",
				"anti_proton",
				"deuteron",
				"triton",
				"alpha",
				"lambda",
				"sigma+",
				"sigma-",
				"xi-",
				"xi0",
				"anti_lambda",
				"anti_sigma+",
				"anti_sigma-",
				"anti_xi-",
				"anti_xi0",
				"omega-",
				"anti_omega-"
	};
}

MyPrimaryGeneratorAction::MyPrimaryGeneratorAction(const bool random) :
  randomize(random)
{
  particleGun = new G4GeneralParticleSource();
  particleTable = G4ParticleTable::GetParticleTable();
}


MyPrimaryGeneratorAction::~MyPrimaryGeneratorAction() {
  delete particleGun;
}


void MyPrimaryGeneratorAction::GeneratePrimaries( G4Event* anEvent ) {
  if (randomize) {
    // Select randomly the primary particle.
    G4int caseBeamParticle =
      static_cast< int >( G4UniformRand() *numberCandidateParticles );
    std::string nameParticle = nameParticlesVector[ caseBeamParticle ];
    particleGun->SetParticleDefinition( particleTable->FindParticle( nameParticle ) );
  }
  particleGun->GeneratePrimaryVertex( anEvent );

}
