# Debugging HepExpMT on the Intel Xeon Phi (MIC)

The following webpage gives details on how to use Intel's special version of
gdb for debugging your application on the MIC:
https://software.intel.com/en-us/articles/debugging-intel-xeon-phi-applications-on-linux-host#Debugging%20Remotely

Here's how I managed to get the remote debugging mode to work. On my MIC card
on mickey.lbl.gov, the gdbserver is already installed, so I started gdb on the
host first with:

    gdb-mic

Then, at the gdb prompt I ran the following commands:

    target extended-remote | ssh -T mic0 LD_LIBRARY_PATH=/home/sfarrell/lib:/home/sfarrell/root_scif_mic/lib ROOTSYS=/home/sfarrell/root_scif_mic LANG=en_US.UTF8 G4LEDATA=/home/sfarrell/G4data/G4EMLOW6.41 G4SAIDXSDATA=/home/sfarrell/G4data/G4SAIDDATA1.1 G4NEUTRONXSDATA=/home/sfarrell/G4data/G4NEUTRONXS1.4 G4LEVELGAMMADATA=/home/sfarrell/G4data/PhotonEvaporation3.1 /usr/bin/gdbserver --multi -
    file /home/sfarrell/G4MT/install-mic/bin/HepExpMT
    set remote exec-file /home/sfarrell/G4MT/install-mic/bin/HepExpMT
    set solib-search-path /opt/intel/composerxe/lib/mic:/home/sfarrell/root_scif_mic/lib
    run /home/sfarrell/G4MT/run_mic/run.mac /home/sfarrell/G4MT/run_mic/cms.gdml 0 1 1 0 Dummy

Note that I set all the needed environment variables in the ssh command,
specified the executable path on both the host and the mic (same in this case),
and told gdb where to find the executables libs *on the host* with
solib-search-path.
