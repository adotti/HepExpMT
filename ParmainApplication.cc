//@WARNINGMESSAGE@
#include <cstdlib>
#include "G4RunManager.hh"
#include "G4MTRunManager.hh"
#include "G4UImanager.hh"
#include "G4UIsession.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "G4UIExecutive.hh"
#include "G4ios.hh"
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#include "CLHEP/Random/MTwistEngine.h"
#include "CLHEP/Random/RanluxEngine.h" 
#include "MyDetectorConstruction.hh" 
#include "MyPrimaryGeneratorAction.hh" 
#include "MyEventAction.hh" 
#include "MyUserActionInitialization.hh"
#include "MyNtuple.hh"
// --- Physics Lists ---
#include "G4PhysListFactory.hh"
#include "MyDummyPL.hh"

// --- ROOT includes ---
#ifdef HEPEXPMT_WITH_ROOT
#ifdef G4MULTITHREADED
#include "TThread.h"
#endif
#endif

// --- MPI ---
#ifdef HEPEXPMT_WITH_MPI
#include "G4MPImanager.hh"
#include "G4MPIsession.hh"
#endif

#include "G4IonTable.hh"
#include "G4NuclearLevelData.hh"

int main(int argc,char** argv) { 
  std::cout<< " ===================================================== " << std::endl;
  std::cout<< " @APPNAME@ Version @MAJORV@.@MINORV@.@PATCHLEVEL@ "<<std::endl;
  std::cout<< "     A stand-alone High Energy Physics Experiments"<<std::endl;
  std::cout<< "     test suite"<<std::endl;
#ifdef HEPEXPMT_WITH_MPI
  std::cout<< "     MPI Interface enabled"<<std::endl;
#endif
#ifdef G4MULTITHREADED
  std::cout<< "     Multi-Threading enabled"<<std::endl;
#endif
      std::cout<< " ===================================================== " << std::endl<<std::endl;


#ifdef HEPEXPMT_WITH_MPI
      // --------------------------------------------------------------------
      // MPI session
      // --------------------------------------------------------------------
      // At first, G4MPImanager/G4MPIsession should be created.
      G4MPImanager* g4MPI = new G4MPImanager(argc, argv);

      // MPI session (G4MPIsession) instead of G4UIterminal
      // Terminal availability depends on your MPI implementation.
      G4MPIsession* session = g4MPI-> GetMPIsession();

      session-> SetPrompt("HepExpMT-MPI");
#endif
  CLHEP::MTwistEngine defaultEngine(1234567);
  G4Random::setTheEngine( &defaultEngine );
  //TODO: Should we make this or use always same seed for reproducibility
  G4long seed = time( NULL );
  G4Random::setTheSeed( seed );

  G4String gdml = "default.gdml";
  G4bool validate = true; //validate schema, turn off if no network
  G4bool perfmem = false; //True measure memory usage
  G4bool randomgun = false;//True randomize primary species
  unsigned long int maxnumhits = 0; //True perform some I/O, maximum number of hits per event
  G4String physlist = "FTFP_BERT";//Default physics list to use

  char* fromEnv=getenv("PHYSLIST");
  if ( fromEnv ) { 
    physlist = G4String(fromEnv);
  }
  static const unsigned int defaultNumThreads = 2;
  if ( argc>2 ) gdml = argv[2];
  if ( argc>3 ) validate = (bool)atoi(argv[3]);
  if ( argc>4 ) perfmem=(bool)atoi(argv[4]);
  if ( argc>5 ) randomgun=(bool)atoi(argv[5]);
  if ( argc>6 ) maxnumhits=strtoul(argv[6],NULL,0);
  if ( argc>7 ) physlist=G4String(argv[7]);
#ifdef HEPEXPMT_WITH_TSCIF
  if ( argc>8 ) MyNtuple::SetChunkSize(atol(argv[8]));
#endif
  G4cout<<" Configuration :"<<G4endl
        <<"   GDML Geometry file: "<<gdml
	<<" (network validation: "<<(validate?"ON":"OFF")<<")"<<G4endl
        <<"   Memory profile: "<<(perfmem?"ON":"OFF")<<G4endl
        <<"   Randomize primary generation: "<<(randomgun?"ON":"OFF")<<G4endl
        <<"   Maximum number of pseudo-hits: "<<maxnumhits<<G4endl
	<<"   Physics List: "<<physlist<<G4endl
	<<"   Default number of threads is: "<<defaultNumThreads<<G4endl
        <<"   Initial RNG engine seed: " << seed << G4endl
#ifdef HEPEXPMT_WITH_TSCIF
        <<"   SCIF Chunk size: "<<MyNtuple::GetChunkSize()<<G4endl
#endif        
	<<G4endl<<" Creating Geant4 RunManager "<< G4endl
	<< G4endl; 

#ifdef G4MULTITHREADED
  G4MTRunManager* runManager = new G4MTRunManager;
  runManager->SetNumberOfThreads( defaultNumThreads );
#else
  G4RunManager* runManager = new G4RunManager;
#endif


//G4NuclearLevelData::GetInstance()->GetParameters()->SetMaxLifeTime( 1.0e+60);
  //Detector
  MyDetectorConstruction* detector = new MyDetectorConstruction(gdml,validate);
  runManager->SetUserInitialization(detector); 

  // --- Physics Lists ---
  if ( physlist != "Dummy" ) {
      G4PhysListFactory factory;
      runManager->SetUserInitialization( factory.GetReferencePhysList(physlist) );
  } else {
      G4cout<<" Instantiating dummy physics list"<<G4endl;
      runManager->SetUserInitialization( new MyDummyPL );
  }
  runManager->SetUserInitialization( new MyUserActionInitialization(perfmem,randomgun,maxnumhits) );

#ifdef G4VIS_USE
   G4VisManager* visManager = new G4VisExecutive;
   visManager->Initialize();
#endif
  
#if defined(G4MULTITHREADED) && defined(HEPEXPMT_WITH_ROOT)
  // Activate thread-safety protections in ROOT
  G4cout << "Activating thread-safety protections in ROOT" << G4endl;
  TThread::Initialize();
#endif
  G4cout<<" Before starting run, size of G4IonTable is "<<G4IonTable::GetIonTable()->Entries() << G4endl;

  G4UImanager* UI = G4UImanager::GetUIpointer();
  if ( argc==1 ) {   // Define UI session for interactive mode.
      G4UIExecutive* ui = new G4UIExecutive(argc,argv);
      UI->ApplyCommand("/control/execute vis.mac");
      ui->SessionStart();
      delete ui;
  } else {   // Batch mode 
    G4String command = "/control/execute "; 
    G4String fileName = argv[1]; 
    UI->ApplyCommand(command+fileName); 
  } 

  G4cout << G4endl 
	 << " ===================================================== " << G4endl 
         << " Final random number = " 
         << CLHEP::HepRandom::getTheEngine()->flat() << G4endl 
	 << " ===================================================== " << G4endl 
         << G4endl; 

  G4cout<<" After run, size of G4IonTable is "<<G4IonTable::GetIonTable()->Entries() << G4endl;
#ifdef G4VIS_USE
  delete visManager;
#endif
#ifdef HEPEXPMT_WITH_MPI
  delete g4MPI;
#endif
  delete runManager;
  return 0; 
} 
